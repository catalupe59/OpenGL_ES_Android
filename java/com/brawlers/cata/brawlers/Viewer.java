package com.brawlers.cata.brawlers;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.brawlers.cata.brawlers.Level.Input;

/**
 * Created by Cata on 9/8/2017.
 */

public class Viewer extends GLSurfaceView {
    private Render mRenderer;

    public Viewer(Context context) {
        super(context);
        setEGLContextClientVersion(2);
        mRenderer = new Render();
        setRenderer(mRenderer);

        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e){
        switch(e.getAction()){
            case MotionEvent.ACTION_DOWN :{
                Input.setTouch(true);
                break;
            }
            case MotionEvent.ACTION_UP:{
                Input.setTouch(false);
                break;
            }
            default:{
                Input.setTouch(false);
                break;
            }
        }
        return true;
    }
}
