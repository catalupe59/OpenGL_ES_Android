package com.brawlers.cata.brawlers.MdlImporter;

import android.util.Pair;

import com.brawlers.cata.brawlers.Program.IO.IO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Cata on 9/12/2017.
 */

public class Parser {
    public static Pair<float[],short[]> mdlToBuffer(String location){
        String mdlCode = IO.LoadTextFile(location);
        String [] lines = mdlCode.split("\n");

        int i = 2;
        for (i =2 ; i < lines.length; i++){
            if (!lines[i].contains("v ")){
                break;
            }
        }
        String[] vertices = Arrays.copyOfRange(lines,2, i);
        for (i =2 ; i < lines.length; i++){
            if (lines[i].contains("f")){
                break;
            }
        }
        String[] elements = Arrays.copyOfRange(lines,i+1,lines.length);

        float[] verticesBuffer = verticesFromLines(vertices);
        short[] elementsBuffer = elementsFromLines(elements);
        return new Pair<float[],short[]>(verticesBuffer, elementsBuffer);
    }

    private static float[] verticesFromLines(String[] lines){
        //Read the V area
        Scanner scn;
        float x = 0;
        ArrayList<Float> buffer = new ArrayList<Float>();
        for(String line : lines){
            scn = new Scanner(line);
            while(!scn.hasNextFloat()){
                scn.next();
            }
            x = scn.nextFloat();
            buffer.add(x);
            while(!scn.hasNextFloat()){
                scn.next();
            }
            x = scn.nextFloat();
            buffer.add(x);
            while(!scn.hasNextFloat()){
                scn.next();
            }
            x = scn.nextFloat();
            buffer.add(x);
        }
        float[] floatBuffer = new float[buffer.size()];
        for (int i = 0; i < buffer.size(); i++){
            floatBuffer[i] = buffer.get(i);
        }
        return floatBuffer;
    }
    private static short[] elementsFromLines(String[] lines){
        //Read the V area
        Scanner scn;
        short x = 0;
        ArrayList<Short> buffer = new ArrayList<Short>();
        for(String line : lines){
            scn = new Scanner(line);
            while(!scn.hasNextShort()){
                scn.next();
            }
            x = scn.nextShort();
            x-=1;
            buffer.add(x);

            while(!scn.hasNextShort()){
                scn.next();
            }
            x = scn.nextShort();
            x-=1;
            buffer.add(x);

            while(!scn.hasNextShort()){
                scn.next();
            }
            x = scn.nextShort();
            x-=1;
            buffer.add(x);

        }

        short[] shortBuffer = new short[buffer.size()];
        for (int i = 0; i < buffer.size(); i++){
            shortBuffer[i] = buffer.get(i);
        }
        return shortBuffer;
    }
}
