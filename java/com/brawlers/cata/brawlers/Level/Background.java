package com.brawlers.cata.brawlers.Level;

import com.brawlers.cata.brawlers.Program.Components.Camera;
import com.brawlers.cata.brawlers.Program.Components.GameObject;
import com.brawlers.cata.brawlers.Program.Components.Texture;
import com.brawlers.cata.brawlers.Program.Components.Transform;
import com.brawlers.cata.brawlers.Program.GameComponent;
import com.brawlers.cata.brawlers.Program.Structs.Vector3f;

/**
 * Created by Cata on 9/13/2017.
 */

public class Background implements GameComponent {

    private GameObject[] backgrounds;

    public Background(){
        Start();
    }

    public void Start(){
        backgrounds = new GameObject[2];
        backgrounds[0] = new GameObject(new Transform(new Vector3f(23f,0,0), new Vector3f(12.5f,16f,1)), new Texture("SeamlessBackground.png"));
        backgrounds[1] = new GameObject(new Transform(new Vector3f(-2,0,0), new Vector3f(12.5f,16f,1)), new Texture("SeamlessBackground.png"));

    }

    private float speed = 0.05f;
    private Vector3f origin = new Vector3f(-23f,0f,0f);
    private Vector3f offset = new Vector3f(-speed,0f,0f);

    public void Update(){
        if (backgrounds[0].transform.getLocation().Distance2D(origin) <= 0.1f){
            backgrounds[0].transform.setLocation(new Vector3f(23,0,0));
        }else {
            backgrounds[0].transform.getLocation().Sum(offset);
        }

        if (backgrounds[1].transform.getLocation().Distance2D(origin) <= 0.1f){
            backgrounds[1].transform.setLocation(new Vector3f(23,0,0));
        }else {
            backgrounds[1].transform.getLocation().Sum(offset);
        }


        backgrounds[0].Update();
        backgrounds[1].Update();
    }
    public void Render(Camera camera){
        backgrounds[0].Render(camera);
        backgrounds[1].Render(camera);
    }
}
