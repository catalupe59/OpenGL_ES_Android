package com.brawlers.cata.brawlers.Level;

import android.util.Log;

import com.brawlers.cata.brawlers.Program.Components.Camera;
import com.brawlers.cata.brawlers.Program.Components.GameObject;
import com.brawlers.cata.brawlers.Program.Components.Texture;
import com.brawlers.cata.brawlers.Program.Components.Transform;
import com.brawlers.cata.brawlers.Program.GameComponent;
import com.brawlers.cata.brawlers.Program.Structs.Vector3f;

/**
 * Created by Cata on 9/13/2017.
 */

public class Player implements GameComponent {
    private GameObject player;

    public Player(){
        Start();
    }

    public void Start(){
        player = new GameObject(new Transform(new Vector3f(-3,0,1), new Vector3f(2,2,0), 0),new Texture("flappy.png"));
    }

    private float delta = -0.15f;

    public void Update(){
        player.transform.getLocation().y -= delta;
        if (Input.getTouch()){
            delta = -0.3f;
        }else {
            delta += 0.01f;
        }
        rotateAfterMid();

        player.Update();
    }

    public void Render(Camera camera){
        player.Render(camera);
    }
    private void rotateAfterMid(){
        float angle = (float)Math.atan2(player.transform.getLocation().y, 15);

        player.transform.setRotation(angle);
    }
}
