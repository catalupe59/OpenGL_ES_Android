package com.brawlers.cata.brawlers.Level;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.util.Log;

import com.brawlers.cata.brawlers.Program.Components.Camera;
import com.brawlers.cata.brawlers.Program.Components.GameObject;
import com.brawlers.cata.brawlers.Program.Components.Texture;
import com.brawlers.cata.brawlers.Program.Components.Transform;
import com.brawlers.cata.brawlers.Program.GameComponent;
import com.brawlers.cata.brawlers.Program.Structs.Vector3f;

/**
 * Created by Cata on 9/13/2017.
 */

public class Level {

    private boolean alive;

    private Background background;
    private Camera camera;
    private Player player;
    private Pipes pipes;

    public Level(){
        Start();
    }

    public void Start(){
        camera = new Camera( new Transform(new Vector3f(0,0,0)), 10,10);
        background = new Background();
        player = new Player();
        pipes = new Pipes();
    }
    public void Reload(int width, int height){
        camera = new Camera( new Transform(new Vector3f(0, 0, 1)), width, height);
    }
    public void Update() {
        camera.Update();
        background.Update();
        player.Update();
        pipes.Update();
    }
    public void Render() {
        background.Render(camera);
        player.Render(camera);
        pipes.Render(camera);
    }
}
