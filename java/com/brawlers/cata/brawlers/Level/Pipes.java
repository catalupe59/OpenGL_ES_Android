package com.brawlers.cata.brawlers.Level;

import com.brawlers.cata.brawlers.Program.Components.Camera;
import com.brawlers.cata.brawlers.Program.Components.GameObject;
import com.brawlers.cata.brawlers.Program.Components.Texture;
import com.brawlers.cata.brawlers.Program.Components.Transform;
import com.brawlers.cata.brawlers.Program.GameComponent;
import com.brawlers.cata.brawlers.Program.Structs.Vector3f;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Cata on 9/13/2017.
 */

public class Pipes implements GameComponent {

    private ArrayList<GameObject> pipes;
    private int currentIndex;

    public Pipes(){
        Start();
    }
    public void Start(){
        pipes = new ArrayList<>();
        //First Pylon
        pipes.add(new GameObject(new Transform(new Vector3f(8,-16,1), new Vector3f(6,9f,1), 0f) , new Texture("pipe.png")));
        pipes.add(new GameObject(new Transform(new Vector3f(8,16,1), new Vector3f(6,9f,1), (float)(Math.PI)) , new Texture("pipe.png")));

        //Second pylon
        pipes.add(new GameObject(new Transform(new Vector3f(26,-16,1), new Vector3f(6,9f,1), 0f) , new Texture("pipe.png")));
        pipes.add(new GameObject(new Transform(new Vector3f(26,16,1), new Vector3f(6,9f,1), (float)(Math.PI)) , new Texture("pipe.png")));
    }

    private float origin = -18;
    private Vector3f offset = new Vector3f(-0.1f, 0, 0);
    public void Update(){
        for (GameObject pipe: pipes) {
            pipe.transform.getLocation().Sum(offset);

            if (pipe.transform.getLocation().x <= origin){
                pipe.transform.setLocation(new Vector3f(18,pipe.transform.getLocation().y, pipe.transform.getLocation().z));
            }

            pipe.Update();
        }
    }
    public void Render(Camera camera){
        for (GameObject pipe: pipes) {
            pipe.Render(camera);
        }
    }
}
