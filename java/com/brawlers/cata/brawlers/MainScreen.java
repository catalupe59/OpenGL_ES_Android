package com.brawlers.cata.brawlers;

import android.app.Activity;
import android.os.Bundle;
import android.content.res.AssetManager;


public class MainScreen extends Activity {

    private Viewer glView;
    private static AssetManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manager = getAssets();
        glView = new Viewer(this);
        setContentView(glView);

    }

    public static AssetManager getManager(){
        return manager;
    }

}
