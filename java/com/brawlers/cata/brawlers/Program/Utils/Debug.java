package com.brawlers.cata.brawlers.Program.Utils;

import android.opengl.GLES20;
import android.util.Log;

/**
 * Created by Cata on 9/12/2017.
 */

public class Debug {
    public static void throwError(String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(glOperation, ": glError " + error);
            throw new RuntimeException(glOperation + ": glError " + error);
        }
    }

}
