package com.brawlers.cata.brawlers.Program.Utils;

import android.opengl.GLES20;
import android.util.Pair;

import java.util.HashMap;

/**
 * Created by Cata on 9/8/2017.
 */

public class Attributes {
    private static HashMap<Pair<Integer,String>,Integer> attributePool = new HashMap<>();

    public static int getAttribute(int shader, String attribName){
        Pair<Integer,String> queryPair = new Pair<>(shader,attribName);
        if (attributePool.containsKey(queryPair)){
            return attributePool.get(queryPair);
        }else {
            int attrib = GLES20.glGetAttribLocation(shader,attribName);
            attributePool.put(queryPair, attrib);
            return attrib;
        }
    }
}
