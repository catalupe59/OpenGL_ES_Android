package com.brawlers.cata.brawlers.Program.Utils;

import android.opengl.GLES20;
import android.util.Pair;

import java.util.HashMap;

/**
 * Created by Cata on 9/12/2017.
 */


public class Uniforms {
    private static HashMap<Pair<Integer,String>,Integer> uniformPool = new HashMap<>();

    public static int getUniforms(int shader, String uniformName){
        Pair<Integer,String> queryPair = new Pair<>(shader,uniformName);
        if (uniformPool.containsKey(queryPair)){
            return uniformPool.get(queryPair);
        }else {
            int uniform = GLES20.glGetUniformLocation(shader,uniformName);
            uniformPool.put(queryPair, uniform);
            return uniform;
        }
    }
}
