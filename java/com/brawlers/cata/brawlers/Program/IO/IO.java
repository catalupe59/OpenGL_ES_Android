package com.brawlers.cata.brawlers.Program.IO;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.brawlers.cata.brawlers.MainScreen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Cata on 9/8/2017.
 */

public class IO {
    public static String LoadTextFile(String location) {
        try {
            InputStream input = MainScreen.getManager().open(location);
            BufferedReader buffrdr = new BufferedReader(new InputStreamReader(input));
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = buffrdr.readLine()) != null){
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            input.close();
            return stringBuffer.toString();
        } catch(IOException e){
            return "";
        }
    }

    public static Bitmap LoadImage(String location){
        Bitmap image = null;
        try{
            InputStream input = MainScreen.getManager().open(location);
            image = BitmapFactory.decodeStream(input);
        }catch(IOException e){

        }
        return image;
    }
}

