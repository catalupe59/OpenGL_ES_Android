package com.brawlers.cata.brawlers.Program.Structs;

import android.util.Log;

/**
 * Created by Cata on 9/8/2017.
 */

public class Vector3f {
    public static final Vector3f up = new Vector3f(0,1,0);
    public static final Vector3f down = new Vector3f(0,-1,0);
    public static final Vector3f left = new Vector3f(-1,0,0);
    public static final Vector3f right = new Vector3f(1,0,0);

    public float x;
    public float y;
    public float z;

    public Vector3f(){
        x = 0;
        y = 0;
        z = 0;
    }
    public Vector3f(float x){
        this.x = x;
        y = 0;
        z =0;
    }
    public Vector3f(float x, float y){
        this.x = x;
        this.y = y;
        z = 0;
    }
    public Vector3f(float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public void Sum(Vector3f vec){
        this.x += vec.x;
        this.y += vec.y;
        this.z += vec.z;
    }
    public void Sum(float scalar){
        this.x += scalar;
        this.y += scalar;
        this.z += scalar;
    }
    public void Product(float scalar){
        this.x *= scalar;
        this.y *= scalar;
        this.z *= scalar;
    }
    public float DotProduct(Vector3f vec){
        return this.x * vec.x + this.y * vec.y + this.z * vec.z;
    }
    public static Vector3f Sum(Vector3f vec1, Vector3f vec2){
        Vector3f vec = new Vector3f(vec1.x + vec2.x, vec1.y + vec2.y, vec1.z + vec2.z);
        return vec;
    }
    public static Vector3f Sum(Vector3f vec1, float scalar){
        Vector3f vec = new Vector3f(vec1.x + scalar, vec1.y + scalar, vec1.z + scalar);
        return vec;
    }
    public static Vector3f Product(Vector3f vec1, float scalar){
        Vector3f vec = new Vector3f(vec1.x * scalar, vec1.y * scalar, vec1.z * scalar);
        return vec;
    }
    public static float DotProduct(Vector3f vec1, Vector3f vec2){
        return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z);
    }
    public float Distance2D(Vector3f vec){
        return (float)Math.sqrt(((this.x-vec.x) * (this.x-vec.x)) + (this.y-vec.y)*(this.y-vec.y));
    }
    public float Magnitude2D(){
        return (float)Math.sqrt( (this.x* this.x) + (this.y *this.y));
    }

}
