package com.brawlers.cata.brawlers.Program;


import com.brawlers.cata.brawlers.Program.Components.Camera;

/**
 * Created by Cata on 9/13/2017.
 */

public interface GameComponent {
    public void Start();
    public void Render(Camera camera);
    public void Update();
}
