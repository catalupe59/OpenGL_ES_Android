package com.brawlers.cata.brawlers.Program.Graphics;

import com.brawlers.cata.brawlers.Program.Utils.Utils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Cata on 9/8/2017.
 */

public class MeshRenderer {
    private final FloatBuffer vertexArrayBuffer;
    private final ShortBuffer elementsBuffer;
    private FloatBuffer textureIndexBuffer;

    private int textureSize;
    private final int elementsSize;
    private final int vertexSize;

    public MeshRenderer(){
        float[] vertexArrayData = new float[] {
                -1,  1f, 0.0f,   // top left
                -1f, -1f, 0.0f,   // bottom left
                1f, -1f, 0.0f,   // bottom right
                1f,  1f, 0.0f }; // top right

        vertexArrayBuffer = Utils.toFloatBuffer(vertexArrayData);

        short[] elementsData = new short[]{
                0,1,2,
                0,2,3
        };

        elementsBuffer = Utils.toShortBuffer(elementsData);

        float[] textureCordsData  = new float[]{
                0.0f, 0.0f,
                0.0f, 1.0f,
                1.0f, 1.0f,
                1.0f, 0.0f
        };
        textureIndexBuffer = Utils.toFloatBuffer(textureCordsData);

        elementsSize = elementsData.length;
        vertexSize = vertexArrayData.length;
        textureSize = textureCordsData.length;

    }
    public MeshRenderer(float [] vertices, short[] elements){
        vertexArrayBuffer = Utils.toFloatBuffer(vertices);
        elementsBuffer = Utils.toShortBuffer(elements);
        elementsSize = elements.length;
        vertexSize = vertices.length;
    }
    public FloatBuffer getVertexBuffer(){
        return vertexArrayBuffer;
    }
    public ShortBuffer getElementsBuffer(){
        return elementsBuffer;
    }
    public FloatBuffer getTextureIndexBuffer(){return textureIndexBuffer;}
    public int getElementsSize(){
        return elementsSize;
    }
    public int getVertexSize(){
        return vertexSize;
    }
}
