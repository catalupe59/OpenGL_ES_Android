package com.brawlers.cata.brawlers.Program.Graphics;

import android.opengl.GLES20;
import com.brawlers.cata.brawlers.Program.IO.IO;

import android.opengl.GLU;
import android.util.Log;
import android.util.Pair;

import java.util.HashMap;

/**
 * Created by Cata on 9/8/2017.
 */

public class Shader {
    private static HashMap<Pair<String,String>,Integer> shaderPool = new HashMap<>();

    private final int shaderID;
    private String vertexCode;
    private String fragmentCode;

    public Shader(String vertexLocation, String fragmentLocation){
        Pair<String, String> queryPair = new Pair<>(vertexLocation, fragmentLocation);
        if (shaderPool.containsKey(queryPair)){
            Log.d("Shader", "Found the key");
            shaderID = shaderPool.get(queryPair);
        }else{
            shaderID = CreateShader(vertexLocation, fragmentLocation);
            shaderPool.put(queryPair,shaderID);
            Log.d("Shader", "Created a new shader");
            Log.d("Shader", shaderID+"");
        }

    }

    public int CreateShader(String vertexLocation, String fragmentLocation){
        int vertexID = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        int fragmentID = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);

        vertexCode = IO.LoadTextFile(vertexLocation);
        fragmentCode =  IO.LoadTextFile(fragmentLocation);
        GLES20.glShaderSource(vertexID, vertexCode);
        GLES20.glShaderSource(fragmentID, fragmentCode);


        GLES20.glCompileShader(vertexID);
        int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(vertexID, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] != GLES20.GL_TRUE) {
            Log.d("Shader error vertex", GLES20.glGetShaderInfoLog(vertexID));
        }

        GLES20.glCompileShader(fragmentID);
        compileStatus = new int[1];
        GLES20.glGetShaderiv(fragmentID, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] != GLES20.GL_TRUE) {
            Log.d("Shader error fragment", GLES20.glGetShaderInfoLog(fragmentID));
        }

        int ID = GLES20.glCreateProgram();
        GLES20.glAttachShader(ID, vertexID);
        GLES20.glAttachShader(ID, fragmentID);
        GLES20.glLinkProgram(ID);
        //Check for errors
        int[] linkstatus = new int[1];
        GLES20.glGetProgramiv(ID, GLES20.GL_LINK_STATUS, linkstatus, 0);
        if (linkstatus[0] != GLES20.GL_TRUE){
            Log.d("Shader error link",GLES20.glGetProgramInfoLog(ID));
        }
        GLES20.glDetachShader(ID, vertexID);
        GLES20.glDetachShader(ID, fragmentID);
        GLES20.glDeleteShader(vertexID);
        GLES20.glDeleteShader(fragmentID);

        return ID;
    }
    @Override
    public String toString(){
        return "Vertex code of "+ vertexCode + "\n fragment code of "+ fragmentCode;
    }
    public int getShaderID(){
        return shaderID;
    }
}
