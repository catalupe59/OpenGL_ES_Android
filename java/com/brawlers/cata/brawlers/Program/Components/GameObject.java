package com.brawlers.cata.brawlers.Program.Components;

import android.util.Pair;

import com.brawlers.cata.brawlers.MdlImporter.Parser;

/**
 * Created by Cata on 9/8/2017.
 */

public class GameObject {

    public Transform transform;
    private Material material;

    public void Update(){
        transform.Update();
    }
    public void Render(Camera camera){
        material.Render(camera, transform);
    }

    public GameObject(){
        setTransform(new Transform());
        setMaterial(new Material());
    }
    public GameObject(Transform transform){
        setTransform(transform);
        setMaterial(new Material());
    }

    public GameObject(Transform transform, String mdlLocation){
        setTransform(transform);
        Pair<float[], short[]> data = Parser.mdlToBuffer(mdlLocation);
        setMaterial(new Material(data.first, data.second));
    }

    public GameObject(Transform transform, Texture texture){
        setTransform(transform);
        setMaterial(new Material(texture));
    }
    public GameObject(Transform transform, Texture texture, String mdlLocation){
        //TODO
    }


    private void setTransform(Transform transform){
        this.transform = transform;
    }
    private void setMaterial(Material material){
        this.material = material;
    }
}
