package com.brawlers.cata.brawlers.Program.Components;

import com.brawlers.cata.brawlers.Program.Structs.Mat4f;

/**
 * Created by Cata on 9/8/2017.
 */

public class Camera {
    private Transform transform;
    private Mat4f projectionMatrix;

    public Camera(Transform transform, int width, int height){
        setTransform(transform);
        setOrthographic(width/100, height/100, 10.0f, 0.1f);
    }

    public void Update(){
        transform.Update();
    }
    public void setTransform(Transform transform){
        this.transform = transform;
    }

    public void setPerspective(float width, float height, float far, float near){
        float left = -width/2;
        float right = width/2;
        float top = height/2;
        float bottom = -height/2;

        projectionMatrix = new Mat4f(new float[]{
                2*near/(right-left), 0, (right+left)/(right-left), 0,
                0, 2*near/(top-bottom), (top+bottom)/(top-bottom), 0,
                0, 0, (near+far)/(near-far), 2*(near*far)/(near-far),
                0,0,-1,0
        });
    }

    public void setOrthographic(float width, float height, float far, float near){
        float left = -width/2;
        float right = width/2;
        float top = height/2;
        float bottom = -height/2;

        projectionMatrix = new Mat4f(new float[]{
                2.0f/(right-left), 0, 0, (right+left)/(left-right),
                0, 2.0f/(top-bottom), 0, (top + bottom)/(bottom-top),
                0, 0, 2.0f/(near-far), (far+near)/(far-near),
                0,0,0,1
        });
    }

    public Mat4f getProjectionMatrix(){
        return projectionMatrix;
    }
    public Transform getTransform(){
        return transform;
    }
}
