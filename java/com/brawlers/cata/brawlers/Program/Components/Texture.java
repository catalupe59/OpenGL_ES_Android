package com.brawlers.cata.brawlers.Program.Components;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.brawlers.cata.brawlers.Program.IO.IO;

import java.nio.IntBuffer;
import java.util.HashMap;

/**
 * Created by Cata on 9/12/2017.
 */

public class Texture {
    private static HashMap<String, int[]> texturePool = new HashMap<>();

    private final int[] texture;

    public Texture (String location){
        if (texturePool.containsKey(location)){
            texture = texturePool.get(location);
        }else{
            texture = new int[1];
            LoadTexture(location);
            texturePool.put(location, texture);
        }
    }
    public void LoadTexture (String location){
        Bitmap image = IO.LoadImage(location);
        GLES20.glGenTextures(1, texture,0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture[0]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, image, 0);
        image.recycle();
    }
    public void Bind(){
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture[0]);
    }
    public void Unbind(){
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }
}
