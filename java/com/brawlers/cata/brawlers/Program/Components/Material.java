package com.brawlers.cata.brawlers.Program.Components;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.util.Log;

import com.brawlers.cata.brawlers.Program.Graphics.MeshRenderer;
import com.brawlers.cata.brawlers.Program.Graphics.Shader;
import com.brawlers.cata.brawlers.Program.Utils.Attributes;
import com.brawlers.cata.brawlers.Program.Utils.Uniforms;
import com.brawlers.cata.brawlers.Program.Utils.Utils;

/**
 * Created by Cata on 9/8/2017.
 */

public class Material {
    private Shader shader;
    private MeshRenderer meshRenderer;
    private Texture texture;

    public Material(){
        setShader("Unlit.vertex", "Unlit.fragment");
        setMeshRenderer(new MeshRenderer());
        setTexture(null);
    }
    public Material(float[] vertices, short[] elements){
        setShader("Unlit.vertex", "Unlit.fragment");
        setMeshRenderer(new MeshRenderer(vertices, elements));
        setTexture(null);
    }
    public Material(Texture texture){
        setShader("Unlit2.vertex", "Unlit2.fragment");
        setMeshRenderer(new MeshRenderer());
        setTexture(texture);
    }
    public Material(float[] vertices, short[] elements, short[] texCoords, Texture texture){
        setShader("Unlit2.vertex", "Unlit2.fragment");
        setMeshRenderer(new MeshRenderer(vertices, elements));
        setTexture(texture);
    }

    public void setTexture(Texture texture){
        this.texture = texture;
    }
    private void setShader(String vertex, String fragment ){
        shader = new Shader(vertex,fragment);
    }

    private void setMeshRenderer(MeshRenderer meshRenderer){
        this.meshRenderer = meshRenderer;
    }
    @Override
    public String toString(){
        return "Material of shader\n"+shader.toString();
    }

    public void Render(Camera camera, Transform transform){
        GLES20.glUseProgram(shader.getShaderID());
        if (texture != null){
            texture.Bind();
        }

        int vPosition = Attributes.getAttribute(shader.getShaderID(), "vPosition");
        int vertSpace = Uniforms.getUniforms(shader.getShaderID(), "vertSpace");
        int cameraSpace = Uniforms.getUniforms(shader.getShaderID(), "camSpace");
        int prMat = Uniforms.getUniforms(shader.getShaderID(), "prMat");

        int textureID = 0;
        int texCoords = 0;
        if (this.texture != null){
            textureID = Uniforms.getUniforms(shader.getShaderID(), "uTexture");
            texCoords = Attributes.getAttribute(shader.getShaderID(), "texCoord");
        }

        GLES20.glEnableVertexAttribArray(vPosition);
        GLES20.glVertexAttribPointer(vPosition,3, GLES20.GL_FLOAT, false, 12, meshRenderer.getVertexBuffer());

        GLES20.glUniformMatrix4fv(vertSpace, 1,false, Utils.toFloatBuffer(transform.getMVP().Transpose().getData()));

        GLES20.glUniformMatrix4fv(cameraSpace, 1,false, Utils.toFloatBuffer(camera.getTransform().getMVP().Transpose().getData()));

        GLES20.glUniformMatrix4fv(prMat, 1,false, Utils.toFloatBuffer(camera.getProjectionMatrix().getData()));


        if (this.texture != null) {
            GLES20.glEnableVertexAttribArray(texCoords);
            GLES20.glVertexAttribPointer(texCoords, 2, GLES20.GL_FLOAT, false, 8, meshRenderer.getTextureIndexBuffer());
            GLES20.glUniform1i(textureID, 0);
        }
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, meshRenderer.getElementsSize() , GLES20.GL_UNSIGNED_SHORT ,meshRenderer.getElementsBuffer());


        if (this.texture != null) {
            GLES20.glDisableVertexAttribArray(texCoords);
        }

        GLES20.glDisableVertexAttribArray(vPosition);

        if (texture!=null){
            texture.Unbind();
        }
        GLES20.glUseProgram(0);



    }

}
