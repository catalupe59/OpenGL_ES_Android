package com.brawlers.cata.brawlers.Program.Components;

import com.brawlers.cata.brawlers.Program.Structs.Mat4f;
import com.brawlers.cata.brawlers.Program.Structs.Vector3f;

/**
 * Created by Cata on 9/8/2017.
 */

public class Transform {
    private Vector3f location;
    private Vector3f size;
    private float angle;

    private Mat4f objectMVP;
    private Mat4f translateMatrix;
    private Mat4f scaleMatrix;
    private Mat4f rotationMatrix;

    public Transform(){
        objectMVP = Mat4f.identity();
        setLocation(new Vector3f(0.0f,0.0f,0.0f));
        setSize(new Vector3f(1.0f,1.0f,1.0f));
        setRotation(0);
    }

    public Transform(Vector3f location){
        objectMVP = Mat4f.identity();
        setLocation(location);
        setSize(new Vector3f(1.0f,1.0f,1.0f));
        setRotation(0);
    }

    public Transform(Vector3f location, Vector3f size){
        objectMVP = Mat4f.identity();
        setLocation(location);
        setSize(size);
        setRotation(0);
    }
    public Transform(Vector3f location ,Vector3f size, float angle){
        objectMVP = Mat4f.identity();
        setLocation(location);
        setSize(size);
        setRotation(angle);
    }

    public Vector3f getLocation(){
        return location;
    }
    public Vector3f getSize(){
        return size;
    }

    public void setRotation(float angle){
        this.angle = angle;
    }
    public void setLocation(Vector3f location){
        this.location = location;
    }
    public void setSize(Vector3f size){
        this.size = size;
    }

    public void Update(){
        translateMatrix = Mat4f.TranslateSt(location);
        scaleMatrix = Mat4f.ScaleSt(size);
        rotationMatrix = Mat4f.RotateSt(angle);

        Mat4f temp = Mat4f.Product(translateMatrix,scaleMatrix );
        objectMVP = Mat4f.Product(temp, rotationMatrix);
//        objectMVP = Mat4f.Product(translateMatrix,scaleMatrix);
    }
    public Mat4f getMVP(){
        return objectMVP;
    }
}
