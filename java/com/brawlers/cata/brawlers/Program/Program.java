package com.brawlers.cata.brawlers.Program;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.util.Log;

import com.brawlers.cata.brawlers.Level.Level;
import com.brawlers.cata.brawlers.Program.Components.GameObject;
import com.brawlers.cata.brawlers.Program.Components.Camera;
import com.brawlers.cata.brawlers.Program.Components.Texture;
import com.brawlers.cata.brawlers.Program.Components.Transform;
import com.brawlers.cata.brawlers.Program.Structs.Vector3f;

/**
 * Created by Cata on 9/8/2017.
 */

public class Program {

    private Level level;

    //Singleton Program
    private Program(){}
    static private Program instance;
    static public Program getInstance(){
        if (instance == null){
            instance = new Program();
        }
        return instance;
    }

    //Start Load method
    public void Start(){
        level = new Level();

    }
    //Reload in case of screen orientation
    public void Reload(int width, int height){
        level.Reload(width, height);
    }
    //Update loop
    private void Update(){
        int x = GLES20.glGetError();
        if (x != GLES20.GL_FALSE) {
            Log.d("Error", GLU.gluErrorString(x));
        }
        level.Update();
    }
    //RenderLoop
    private void Render() {
        level.Render();
    }

    public void Run(){
        Update();
        Render();
    }
}
